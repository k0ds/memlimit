//MemLimit: Write A tool called MemLimit that accepts a process id and a number representing a maximum commited memory for the process and set that limit using a job.

#include <iostream>
#include <stdio.h>
#include <Windows.h>

#define _WIN32_WINNT 0x0500

int main()
{
	
	SetConsoleTitle(L"MemLimit");
	char PidBuf[256];
	char MemLimitBuf[256];


	printf("\nEnter A pid:  ");
	fgets(PidBuf, sizeof(PidBuf), stdin);
	int pid = atoi(PidBuf);
	printf("\nEnter The memory limt in MB: ");
	fgets(MemLimitBuf, sizeof(MemLimitBuf), stdin);
	int MemLimit = atoi(MemLimitBuf);


	HANDLE hJob = ::CreateJobObject(nullptr, L"MemLimitJob");
	if (!hJob)
	{
		printf("Failed to create Job\n");
		return 1;
	}
	HANDLE hProcess = ::OpenProcess(PROCESS_SET_QUOTA | PROCESS_TERMINATE, FALSE, pid); //open process to be added to job
	if (!hProcess)
	{
		printf("Failed to get handle to process %d (Error = %d)\n", pid, ::GetLastError());
		return 1;
	}
	
	if (!::AssignProcessToJobObject(hJob, hProcess))  //add process to job
	{
		printf("Failed to assign process %d to job (error=%d)\n", pid, ::GetLastError());
		return 1;
	} 
	else
	{
		printf("Added process %d to job\n", pid);
	}
	::CloseHandle(hProcess);

	size_t PeakMemoryUsed = 0;

	JOBOBJECT_BASIC_LIMIT_INFORMATION basicInfo;
	basicInfo.LimitFlags = JOB_OBJECT_LIMIT_PROCESS_MEMORY;
	JOBOBJECT_EXTENDED_LIMIT_INFORMATION info; 
	info.BasicLimitInformation = basicInfo;
	info.ProcessMemoryLimit = MemLimit;
	info.JobMemoryLimit = MemLimit; //set the memory limit
	info.PeakJobMemoryUsed = PeakMemoryUsed;

	



	

	if (!::SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &info, sizeof(info))) //set the limit to the process
	{
		printf("Error setting memory limit. ERROR : %d\n\n", ::GetLastError()); 
		return 1;
	}
	else
	{
		printf("Memory limit %d on PID %d set successfully!\n\n", MemLimit, pid);
		printf("Peak memory used by job: %llu\n", PeakMemoryUsed);
		return 0;
	}

	printf("Press enter to quit.\n");
	char dummy[10];
	gets_s(dummy);

	::CloseHandle(hJob);
	return 0;



}